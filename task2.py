from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import random

driver = webdriver.Chrome(executable_path=r'C:\Users\baldezhh\Desktop\practice\z_gh_driver\chromedriver.exe')
url = 'https://dekanat.nung.edu.ua/cgi-bin/timetable.cgi'
prepod = 'Мельничук Степан Іванович'
gruppa = 'КІ-19-1'
data_start = '01.11.2021'
data_end = '01.12.2021'

try:
    driver.get(url=url)
    time.sleep(2)
    driver.find_element(By.ID, 'faculty').click()
    time.sleep(0.3)
    driver.find_element(By.XPATH, '//*[@id="faculty"]/option[7]').click()
    time.sleep(0.3)
    write = driver.find_element(By.XPATH, '//*[@id="teacher"]')
    group = driver.find_element(By.XPATH, '//*[@id="group"]')

    for item in prepod:
        # time.sleep(random.uniform(0.05, 0.5))
        write.send_keys(item)

    # for item in gruppa:
    #     time.sleep(random.uniform(0.05, 0.5))
    #     group.send_keys(item)

    driver.find_element(By.XPATH, '//*[@id="wrap"]/div/div/div/div[2]/form/div[3]/div[3]/button').click()
    time.sleep(10)
except Exception as ex:
    print(ex)
finally:
    driver.close()
    driver.quit()
